//
//  TermsVC.swift
//  Observe
//
//  Created by USER on 22/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TermsVC: UIViewController,UIWebViewDelegate {
    
    
    @IBOutlet weak var wait_lbl: UILabel!

    
    @IBAction func BACK(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBOutlet weak var Web_vw: UIWebView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        ServiceTerms()
        Web_vw.delegate = self                // Do any additional setup after loading the view.
                      }
                      
                      func ServiceTerms()
                      {
                        let type = NVActivityIndicatorType.ballRotate
                                                                   let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                                   let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                                          self.view.addSubview(activityIndicatorView)
                                                         self.view.isUserInteractionEnabled = false

                          activityIndicatorView.startAnimating()
                          var param = [String:Any]()
                          
                          
                          print("param",param)
                          
                          WebService().postRequest(methodName: terms , parameter: param) { (response) in
                              print("response",response)
                              
                              activityIndicatorView.stopAnimating()
                              self.view.isUserInteractionEnabled = true

                              if let newResponse = response as? NSDictionary {
                                  if newResponse.value(forKey: "status") as! Bool  == true {
                                      
                                      let data = newResponse.value(forKey: "data") as! NSArray
                                      
                                    let descr = data.value(forKey: "description") as! NSArray
                                    
                                      let descriptionStr = descr[0] as! String
                                      let descFinal = descriptionStr.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
                                      let descFinal2 = descFinal.replacingOccurrences(of: "\n", with: " ").replacingOccurrences(of: "\"", with: "")
                                    //  let content = "<html><body><p><font size=30> \(descFinal2)</font></p></body></html>"
                                      
                                     // let test = String(content.filter { !" \n\t\r".contains($0) })
                                      
              //                        self.web_view.loadHTMLString(content, baseURL: nil)
              //                        print("content",content)
                                      let descWithouthtml = descriptionStr.withoutHtml as! String
                                      
                                 let content = "<html><body><p><font size=4> \(descriptionStr)</font></p></body></html>"

                                                                                 self.Web_vw.loadHTMLString(content, baseURL: nil)
                                      
                                  }
                                  else {
                                      self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                  }
                              }
                              else if let newMsg = response as? String{
                                  self.view.showToast(toastMessage: newMsg, duration: 1)

                              }
                              else {
                                  
                                  self.view.showToast(toastMessage: "No Data Found", duration: 1)

                              }
                          }
                          
                      }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.wait_lbl.isHidden = true
    }
    

              }
            
extension String {
    public var withoutHtml: String {
        guard let data = self.data(using: .utf8) else {
            return self
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }
        
        return attributedString.string
    }
}
