//
//  ContactSVC.swift
//  Observe
//
//  Created by USER on 21/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit

class ContactSVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    // MARK: - IBOutlets :
      
      @IBOutlet weak var noAccess_View: UIView!

    @IBOutlet weak var contacts_list: UITableView!
    

      // MARK: - IBActions :

      
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UserDefaults.standard.value(forKey: "userIdLogin") != nil
                                       {
                                          self.noAccess_View.isHidden = true

                                       }
                                       else
                                       {
                                          
                                          self.noAccess_View.isHidden = false
                                                                                                                

                                       }

        
        
              contacts_list.delegate = self
          contacts_list.dataSource = self
          contacts_list.separatorStyle = .none
        
    SetStsBar()
                }
                func SetStsBar()
                {
                   if #available(iOS 13.0, *) {
                                                   let app = UIApplication.shared
                                                   let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                                   
                                                   let statusbarView = UIView()
                                       statusbarView.backgroundColor = .white
                                                   view.addSubview(statusbarView)
                                                 
                                                   statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                                   statusbarView.heightAnchor
                                                       .constraint(equalToConstant: statusBarHeight).isActive = true
                                                   statusbarView.widthAnchor
                                                       .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                                   statusbarView.topAnchor
                                                       .constraint(equalTo: view.topAnchor).isActive = true
                                                   statusbarView.centerXAnchor
                                                       .constraint(equalTo: view.centerXAnchor).isActive = true
                                                 
                                               } else {
                                                   let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                                   statusBar?.backgroundColor =  .white
                                               }
                   }
           
    // MARK: - Table view methods:-
       
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 150
       }
       
       
       
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 2
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = contacts_list.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
           
           cell.selectionStyle = .none
           return cell
           
           
       }
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
       let number = "9090909090"

           if let url = URL(string: "tel://\(number)"),
                      UIApplication.shared.canOpenURL(url) {
                      if #available(iOS 10, *) {
                          UIApplication.shared.open(url, options: [:], completionHandler:nil)
                      } else {
                          UIApplication.shared.openURL(url)
                      }
                  } else {
               self.view.showToast(toastMessage: "Call error", duration: 1)

                  }
           
           
       }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
