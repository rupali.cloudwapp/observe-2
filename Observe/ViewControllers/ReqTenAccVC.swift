//
//  ReqTenAccVC.swift
//  Observe
//
//  Created by USER on 20/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView

@available(iOS 13.0, *)
class ReqTenAccVC: UIViewController {
   
    
    
     // MARK: - IBDeclarations :
     
    var tenfromBool = false
    var tenToBool = false

    var tenfromDate = Date()
     var PropertyIdArr = [String]()
     var blockunitIdArr = [String]()
     var PropertyNameArr = [String]()
    
    // MARK: - IBOutlets :
    
    
    @IBOutlet weak var name_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var phoneNum_tf: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var property_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var email_tf: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var pw_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tenfrom_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tento_tf: SkyFloatingLabelTextField!
    // MARK: - IBActions :

    @IBAction func tento_act(_ sender: Any) {
        
       
        
        if self.tenfromBool == false
        {
            
  self.view.showToast(toastMessage: "Please selct tenant from first", duration: 1)
            
        }
        else
        {
       let currentDate = Date()
                           var mindateComponents = DateComponents()
                          // mindateComponents.year = self.tenfromDate
               
                           var maxdateComponents = DateComponents()
                           maxdateComponents.year = 51
                           let minDate = Calendar.current.date(byAdding: mindateComponents, to: currentDate)
                           let maxDate = Calendar.current.date(byAdding: maxdateComponents, to: currentDate)
               
                           let datePicker = DatePickerDialog(textColor: UIColor.black,
                                                             buttonColor:UIColor.black ,
                                                             font: UIFont.systemFont(ofSize: 14),
                                                             showCancelButton: true)
               
               
                           datePicker.show("Please Select Tenant to Date",
                                           doneButtonTitle: "Done",
                                           cancelButtonTitle: "Cancel",
                                           minimumDate: self.tenfromDate,
                                           maximumDate:maxDate ,
                                           datePickerMode: .date) { (date) in
                                               if let dt = date {
                                                   let formatter2 = DateFormatter()
                                                   formatter2.dateFormat = "yyyy-MM-dd"
               
                                                   let str =   formatter2.string(from: dt)
                                                   
                                                   print("str",str)
                                                   self.tento_tf.text = str
                                               }
                           }
            
        }
    }
    
    @IBAction func tenfrom_act(_ sender: Any) {
        let currentDate = Date()
                           var mindateComponents = DateComponents()
                           mindateComponents.year = 0
               
                           var maxdateComponents = DateComponents()
                           maxdateComponents.year = 50
                           let minDate = Calendar.current.date(byAdding: mindateComponents, to: currentDate)
                           let maxDate = Calendar.current.date(byAdding: maxdateComponents, to: currentDate)
               
                           let datePicker = DatePickerDialog(textColor: UIColor.black,
                                                             buttonColor:UIColor.black ,
                                                             font: UIFont.systemFont(ofSize: 14),
                                                             showCancelButton: true)
               
               
                           datePicker.show("Please Select Tenant from Date",
                                           doneButtonTitle: "Done",
                                           cancelButtonTitle: "Cancel",
                                           minimumDate: currentDate,
                                           maximumDate:maxDate ,
                                           datePickerMode: .date) { (date) in
                                               if let dt = date {
                                                   let formatter2 = DateFormatter()
                                                   formatter2.dateFormat = "yyyy-MM-dd"
               
                                                   let str =   formatter2.string(from: dt)
                                                self.tenfromDate = dt
                                                self.tenfromBool = true

                                                   print("str",str)
                                                   self.tenfrom_tf.text = str
                                               }
                           }
    }
    
    @IBAction func property_act(_ sender: Any) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyListVC") as! PropertyListVC
               
         self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @IBAction func back_act(_ sender: Any) {
                                            UserDefaults.standard.removeObject(forKey: "PropertyIdArr")

        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submit_act(_ sender: Any) {
        
        
               if name_tf.text!.isEmpty {
                   self.view.showToast(toastMessage:  "Name Required!!", duration: 1)
                                                          }
                   
                 else if !(email_tf.text!.isValidEmail()) {
               self.view.showToast(toastMessage:  "Valid Email Required!!", duration: 1)
                                                      }
                   else if property_tf.text!.isEmpty {
                              
                              self.view.showToast(toastMessage:  "Property Required!!", duration: 1)
                              
                              }
               else if phoneNum_tf.text!.isEmpty {
                   
                   self.view.showToast(toastMessage:  "Phone Number Required!!", duration: 1)
                   
                   }
                   else if pw_tf.text!.isEmpty {
                   
                   self.view.showToast(toastMessage:  "Password Required!!", duration: 1)
                   
                   }
        else if tenfrom_tf.text!.isEmpty {
        
        self.view.showToast(toastMessage:  "Time from Required!!", duration: 1)
        
        }
        else
               {
                
                self.ReqTenant()
                
        }
        
        
        
        
        
    }
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

         SetStsBar()
         }
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            }
    
    override func viewWillAppear(_ animated: Bool) {
            
           if UserDefaults.standard.value(forKey: "PropertyIdArr") != nil
           {
               
               PropertyIdArr = UserDefaults.standard.value(forKey: "PropertyIdArr") as! [String]
               blockunitIdArr = UserDefaults.standard.value(forKey: "blockunitIdArr") as! [String]
               PropertyNameArr = UserDefaults.standard.value(forKey: "PropertyNameArr") as! [String]
              
               let seperatedColumns = self.PropertyNameArr.joined(separator: ",")

               self.property_tf.text = seperatedColumns
               
           }
           else
           {
               
           }
       }
    
    
    
     // MARK: - RequestAcc Signup :

        func  ReqTenant()
              {
                   let type = NVActivityIndicatorType.ballRotate
                                          let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                          let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                 self.view.addSubview(activityIndicatorView)
                                self.view.isUserInteractionEnabled = false
                   
                  
                       activityIndicatorView.startAnimating()
                       var param = [String:Any]()
             

                param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")

                param["name"] = self.name_tf.text as! String
               param["email"] =  self.email_tf.text as! String
               param["mobile"] = self.phoneNum_tf.text as! String
               param["property_ids"] = PropertyIdArr.joined(separator: ",")
               param["block_unit_id"] = blockunitIdArr.joined(separator: ",")
                param["password"] = self.pw_tf.text as! String
                param["tenant_from"] = self.tenfrom_tf.text as! String
                param["tenant_to"] = self.tento_tf.text as! String

               



                       WebService().postRequest(methodName: tenant_add , parameter: param) { (response) in
                           
                           activityIndicatorView.stopAnimating()
                          self.view.isUserInteractionEnabled = true
                           
                           if let newResponse = response as? NSDictionary {
                               if newResponse.value(forKey: "status") as! Bool  == true {
                                 //  if let userData = newResponse.value(forKey: "data") as? NSDictionary {
                                        
                                 // print("userData",userData)
                                       
                                
                                    UserDefaults.standard.removeObject(forKey: "PropertyIdArr")
                                    
                                             UserDefaults.standard.set(false, forKey: "FromSignup")
                                              UserDefaults.standard.set(false, forKey: "FromProfile")


                                                  let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReqThankYouVC") as? ReqThankYouVC
                                                                                                       let transition:CATransition = CATransition()
                                              transition.duration = 0.8
                                              transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                                                                                transition.type = CATransitionType.push
                                                                                transition.subtype =  CATransitionSubtype.fromTop
                                                                                                       self.navigationController!.view.layer.add(transition, forKey: kCATransition)
                                                                                                       self.navigationController?.pushViewController(vc!, animated: false)
                                                                                                               
                                  // }
                               }
                               else {
                                   self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                               }
                           }
                           else if let newMsg = response as? String{
                              self.view.showToast(toastMessage: newMsg, duration: 1)
                           }
                           else {
                              self.view.showToast(toastMessage: "No Data Found", duration: 1)

                           }
                       }
                   
              }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
