//
//  TenanatAccDetailVC.swift
//  Observe
//
//  Created by USER on 20/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class TenanatAccDetailVC: UIViewController {
    
    
    // MARK: - IBOoutlets :-

    @IBOutlet weak var name_tf: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var phoneNum_tf: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var address_tf: SkyFloatingLabelTextFieldWithIcon!
    
 
    @IBOutlet weak var email_tf: SkyFloatingLabelTextFieldWithIcon!
    
    
    
    
       // MARK: - IBActions :-
             
             
  @IBAction func back(_ sender: Any) {
                 self.navigationController?.popViewController(animated: true)
             }
    
    
    @IBAction func del_act(_ sender: Any) {
    }
    
    
    
    @IBAction func editDuration_act(_ sender: Any) {
    }
    
    
    @IBAction func editName_act(_ sender: Any) {
        
        name_tf.becomeFirstResponder()
        
        name_tf.isUserInteractionEnabled = true

    }
    
    
    @IBAction func editNumber_act(_ sender: Any) {
        phoneNum_tf.becomeFirstResponder()

        phoneNum_tf.isUserInteractionEnabled = true

    }
    
    @IBAction func editAddress_act(_ sender: Any) {
        address_tf.becomeFirstResponder()

        address_tf.isUserInteractionEnabled = true

    }
    
    @IBAction func editEmail_act(_ sender: Any) {
        email_tf.becomeFirstResponder()

        email_tf.isUserInteractionEnabled = true

    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        name_tf.isUserInteractionEnabled = false
           
        phoneNum_tf.isUserInteractionEnabled = false
           
        address_tf.isUserInteractionEnabled = false
           
        email_tf.isUserInteractionEnabled = false

           SetStsBar()
         }
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
