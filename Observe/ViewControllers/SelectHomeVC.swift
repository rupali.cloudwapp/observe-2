//
//  SelectHomeVC.swift
//  Observe
//
//  Created by USER on 23/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SelectHomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource  {

    
    var PropertyDataArr = NSArray()
    
    
    @IBOutlet weak var list_table: UITableView!
       
       @IBOutlet weak var list_height: NSLayoutConstraint!
       // MARK: - IBActions :-
             
    
            @IBAction func back(_ sender: Any) {
                         self.navigationController?.popViewController(animated: true)
                     }
            
            
            
            
            override func viewDidLoad() {
                super.viewDidLoad()
                
                list_table.delegate = self
                list_table.dataSource = self
                list_table.separatorStyle = .none


                        UserPropertyList()

            }
            // MARK: - WebService Property list :
             func  UserPropertyList()
                     {
                          let type = NVActivityIndicatorType.ballRotate
                                                 let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                 let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                        self.view.addSubview(activityIndicatorView)
                                       self.view.isUserInteractionEnabled = false
                          
                         
                              activityIndicatorView.startAnimating()
                              var param = [String:Any]()
                        
                        param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")

                    
                         WebService().postRequest(methodName: user_property_list , parameter: param) { (response) in
                             

                             
                                  
                                  activityIndicatorView.stopAnimating()
                                 self.view.isUserInteractionEnabled = true
                                  
                                  if let newResponse = response as? NSDictionary {
                                      if newResponse.value(forKey: "status") as! Bool  == true {
                                          if let propertyData = newResponse.value(forKey: "data") as? NSArray {
                                             print("propertyData",propertyData)
                                            
                                            self.PropertyDataArr = propertyData
        
                                            self.list_table.reloadData()

                                            
                                        
                                      }
                                      else {
                                          self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                      }
                                  }
                                  }
                                  else if let newMsg = response as? String{
                                     self.view.showToast(toastMessage: newMsg, duration: 1)
                                  }
                                  else {
                                     self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                  }
                              }
                          
                     }

              // MARK: - TABLE VIEW METHODS :-
                func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                    
                    return 120
                    
                }
                
                func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                    return PropertyDataArr.count
                }
                
                func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell

                   let dict = PropertyDataArr[indexPath.row] as! NSDictionary
                    
                    let name = dict.value(forKey: "property_name") as! String
                    let block = dict.value(forKey: "block") as! String
                    let unit = dict.value(forKey: "units") as! String

                   cell.heading_lbl.text = name
                    
                    cell.desc_lbl.text = "Block " + block + " , " + "Unit " + unit
                   

                   cell.selectionStyle = .none
                      
                    return cell
                
   }
               
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

                 let dict = PropertyDataArr[indexPath.row] as! NSDictionary

                    UserDefaults.standard.set(dict, forKey: "SelectedProperty")
        self.navigationController?.popViewController(animated: true)

     }    /*

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

 }*/
}
