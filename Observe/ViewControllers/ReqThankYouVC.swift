//
//  ReqThankYouVC.swift
//  Observe
//
//  Created by USER on 20/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ReqThankYouVC: UIViewController {
    
    
    
    
    // MARK: - IBOutlets :
    
    @IBOutlet weak var close_btn: UIButton!
    
    @IBOutlet weak var heading_label: UILabel!
    

    @IBOutlet weak var hvalook_height: NSLayoutConstraint!
    // MARK: - IBActions :

    
    
    @IBAction func close_act(_ sender: Any) {

        
        if UserDefaults.standard.bool(forKey: "FromSignup") == true
        {
            if #available(iOS 13.0, *) {
                                                                                                   

                                                                             let scene = UIApplication.shared.connectedScenes.first
                                                                  if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                                    sd.setHomeRootController()
                                                                                                     }
                                                                        }
                                                        else
                                                                                                     
                                            {
                                                                                                             
                                appDele.setHomeRootController()
                                                                                                             
                            }
            
        }
        else
        {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TenantAccVC") as! TenantAccVC
//                                          
//               self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.popToRootViewController(animated: true)

        }
       
        
    }
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
              if UserDefaults.standard.bool(forKey: "FromSignup") == true
              {
                heading_label.text = "We will send the account confirmation details to your email.Please allow upto 2 working days for the account."
                
                close_btn.setTitle("Go To Home", for: .normal)
                if UserDefaults.standard.bool(forKey: "FromProfile") == true
                              {
                                  close_btn.isHidden = true
                              }
                              else{
                                  
                              }
        }
        
        else
              {
                hvalook_height.constant = 0
                
                
              
                
              // heading_label.text = "We will send the account confirmation details to your tenant's email.Please allow upto 2 working days for the account."
        }
        

          SetStsBar()
         }
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
