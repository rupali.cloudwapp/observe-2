//
//  NoticeDetailVC.swift
//  Observe
//
//  Created by USER on 20/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit

class NoticeDetailVC: UIViewController {
    
    var dict = NSDictionary()
    
    // MARK: - IBOoutlets :-
          
    @IBOutlet weak var header_lbl: UILabel!
    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var img_vw: UIImageView!
          
          @IBOutlet weak var detail_tv: UITextView!
          
         // MARK: - IBActions :-
          
          
          @IBAction func back(_ sender: Any) {
              self.navigationController?.popViewController(animated: true)
          }
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        SetStsBar()
        
        let image = dict.value(forKey: "image") as! String
                                                   
        self.img_vw.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
        
        self.title_lbl.text = dict.value(forKey: "title") as! String
        self.header_lbl.text = dict.value(forKey: "title") as! String

        self.detail_tv.text = dict.value(forKey: "description") as! String

        
         }
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
