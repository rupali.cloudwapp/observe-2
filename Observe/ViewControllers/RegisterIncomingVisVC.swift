//
//  RegisterIncomingVisVC.swift
//  Observe
//
//  Created by USER on 22/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView

class RegisterIncomingVisVC: UIViewController {
    
    // MARK: - IBOoutlets :-
     
    
    
    @IBOutlet weak var name_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var vehiRegiNum_tf: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var date_tf: SkyFloatingLabelTextField!
    @IBOutlet weak var estimArrivalTime_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var estimDeparture_tf: SkyFloatingLabelTextField!
    @IBOutlet weak var purpose_tf: SkyFloatingLabelTextField!
    
       // MARK: - IBActions :-
             
             
       @IBAction func back(_ sender: Any) {
                 self.navigationController?.popViewController(animated: true)
             }
       
    @IBAction func submit_act(_ sender: Any) {
        
        if name_tf.text!.isEmpty {
                   self.view.showToast(toastMessage:  "Vehicle Name Required!!", duration: 1)
                                                          }
                   
               
                   else if vehiRegiNum_tf.text!.isEmpty {
                              
                              self.view.showToast(toastMessage:  "Vehicle Registration Number Required!!", duration: 1)
                              
                              }
               else if date_tf.text!.isEmpty {
                   
                   self.view.showToast(toastMessage:  "Vehicle date Required!!", duration: 1)
                   
                   }
                   else if estimArrivalTime_tf.text!.isEmpty {
                   
                   self.view.showToast(toastMessage:  "Estimated arival time Required!!", duration: 1)
                   
                   }
        else if estimDeparture_tf.text!.isEmpty {
        
        self.view.showToast(toastMessage:  "Estimated departure time Required!!", duration: 1)
        
        }
        else
               {
                
                self.AddVisitor()
                
        }
    }
    
    
    @IBAction func date_act(_ sender: Any) {
                let currentDate = Date()
                    var mindateComponents = DateComponents()
                    mindateComponents.year = -50
        
                    var maxdateComponents = DateComponents()
                    maxdateComponents.year = 0
                    let minDate = Calendar.current.date(byAdding: mindateComponents, to: currentDate)
                    let maxDate = Calendar.current.date(byAdding: maxdateComponents, to: currentDate)
        
                    let datePicker = DatePickerDialog(textColor: UIColor.black,
                                                      buttonColor:UIColor.black ,
                                                      font: UIFont.systemFont(ofSize: 14),
                                                      showCancelButton: true)
        
        
                    datePicker.show("Please Select Vehicle Date",
                                    doneButtonTitle: "Done",
                                    cancelButtonTitle: "Cancel",
                                    minimumDate: currentDate,
                                    maximumDate:maxDate ,
                                    datePickerMode: .date) { (date) in
                                        if let dt = date {
                                            let formatter2 = DateFormatter()
                                            formatter2.dateFormat = "yyyy-MM-dd"
        
                                            let str =   formatter2.string(from: dt)
                                            
                                            print("str",str)
                                            self.date_tf.text = str
                                        }
                    }
    }
    
    
    @IBAction func arrival_act(_ sender: Any) {
        let currentDate = Date()
                           var mindateComponents = DateComponents()
                           mindateComponents.year = 0
               
                           var maxdateComponents = DateComponents()
                           maxdateComponents.year = 50
                           let minDate = Calendar.current.date(byAdding: mindateComponents, to: currentDate)
                           let maxDate = Calendar.current.date(byAdding: maxdateComponents, to: currentDate)
               
                           let datePicker = DatePickerDialog(textColor: UIColor.black,
                                                             buttonColor:UIColor.black ,
                                                             font: UIFont.systemFont(ofSize: 14),
                                                             showCancelButton: true)
               
               
                           datePicker.show("Please Select Arrival Time",
                                           doneButtonTitle: "Done",
                                           cancelButtonTitle: "Cancel",
                                           minimumDate: currentDate,
                                           maximumDate:maxDate ,
                                           datePickerMode: .time) { (date) in
                                               if let dt = date {
                                                   let formatter2 = DateFormatter()
                                                   formatter2.dateFormat = "HH:mm"
               
                                                   let str =   formatter2.string(from: dt)
                                                   
                                                   print("str",str)
                                                   self.estimArrivalTime_tf.text = str
                                               }
                           }
    }
    
    @IBAction func departure_act(_ sender: Any) {
        let currentDate = Date()
                                  var mindateComponents = DateComponents()
                                  mindateComponents.year = 0
                      
                                  var maxdateComponents = DateComponents()
                                  maxdateComponents.year = 50
                                  let minDate = Calendar.current.date(byAdding: mindateComponents, to: currentDate)
                                  let maxDate = Calendar.current.date(byAdding: maxdateComponents, to: currentDate)
                      
                                  let datePicker = DatePickerDialog(textColor: UIColor.black,
                                                                    buttonColor:UIColor.black ,
                                                                    font: UIFont.systemFont(ofSize: 14),
                                                                    showCancelButton: true)
                      
                      
                                  datePicker.show("Please Select Departure Time",
                                                  doneButtonTitle: "Done",
                                                  cancelButtonTitle: "Cancel",
                                                  minimumDate: currentDate,
                                                  maximumDate:maxDate ,
                                                  datePickerMode: .time) { (date) in
                                                      if let dt = date {
                                                          let formatter2 = DateFormatter()
                                                          formatter2.dateFormat = "HH:mm"
                      
                                                          let str =   formatter2.string(from: dt)
                                                          
                                                          print("str",str)
                                                          self.estimDeparture_tf.text = str
                                                      }
                                  }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - RequestAcc ADD :

       func  AddVisitor()
             {
                  let type = NVActivityIndicatorType.ballRotate
                                         let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                         let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                self.view.addSubview(activityIndicatorView)
                               self.view.isUserInteractionEnabled = false
                  
                 
                      activityIndicatorView.startAnimating()
                      var param = [String:Any]()
            
              
               param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
               param["name"] = self.name_tf.text as! String
              param["registration_no"] =  self.vehiRegiNum_tf.text as! String
              param["v_date"] = self.date_tf.text as! String
              param["arrival_time"] = self.estimArrivalTime_tf.text as! String
               param["departure_time"] = self.estimDeparture_tf.text as! String
               param["purpose"] = self.purpose_tf.text as! String

              



                      WebService().postRequest(methodName: visitor_add , parameter: param) { (response) in
                          
                          activityIndicatorView.stopAnimating()
                         self.view.isUserInteractionEnabled = true
                          
                          if let newResponse = response as? NSDictionary {
                              if newResponse.value(forKey: "status") as! Bool  == true {
                                 // if let userData = newResponse.value(forKey: "data") as? NSDictionary {
                                       
                              //   print("userData",userData)
                                      
          self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)
                
                                    self.name_tf.text = ""
                                   self.vehiRegiNum_tf.text = ""
                                     self.date_tf.text = ""
                                     self.estimArrivalTime_tf.text = ""
                                     self.estimDeparture_tf.text = ""
                                     self.purpose_tf.text = ""
                                //  }
                              }
                              else {
                                  self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                              }
                          }
                          else if let newMsg = response as? String{
                             self.view.showToast(toastMessage: newMsg, duration: 1)
                          }
                          else {
                             self.view.showToast(toastMessage: "No Data Found", duration: 1)

                          }
                      }
                  
             }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
