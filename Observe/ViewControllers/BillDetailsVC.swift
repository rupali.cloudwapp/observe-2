//
//  BillDetailsVC.swift
//  Observe
//
//  Created by USER on 20/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class BillDetailsVC: UIViewController {
    
    
    // MARK: - IBDeclarations :-


     var timebool = Bool()
    var datebool = Bool()

    
    // MARK: - IBOoutlets :-

   
    @IBOutlet weak var item_tf: SkyFloatingLabelTextField!
    @IBOutlet weak var date_tf: SkyFloatingLabelTextField!
    @IBOutlet weak var dueDate_tf: SkyFloatingLabelTextField!
    @IBOutlet weak var billto_tf: SkyFloatingLabelTextField!
    @IBOutlet weak var payto_tf: SkyFloatingLabelTextField!
    
    
    // MARK: - IBActions :-
          
          
    @IBAction func back(_ sender: Any) {
              self.navigationController?.popViewController(animated: true)
          }
    @IBAction func share_act(_ sender: Any) {
      }
      
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

          SetStsBar()
         }
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            
            
            
            item_tf.text = "Service charges for August 2020"

            date_tf.text = "28/9/2020"
                   dueDate_tf.text = "30 September 2020"
                   billto_tf.text = "Richard Wong Eng Eng"
            
            payto_tf.text = "Lagenda Management"

            
            
            }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
