//
//  BookingDetailsVC.swift
//  Observe
//
//  Created by USER on 20/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class BookingDetailsVC: UIViewController {
  
    // MARK: - IBDeclarations :-


     var timebool = Bool()
    var datebool = Bool()

    
    // MARK: - IBOoutlets :-

    @IBOutlet weak var date_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var time_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var bookby_tf: SkyFloatingLabelTextField!
    
    // MARK: - IBActions :-
          
          
    @IBAction func back(_ sender: Any) {
              self.navigationController?.popViewController(animated: true)
          }
    
    
    
    @IBAction func cancel_act(_ sender: Any) {
    }
    
    
    @IBAction func date_act(_ sender: Any) {
//        let currentDate = Date()
//              var mindateComponents = DateComponents()
//              mindateComponents.year = 0
//
//              var maxdateComponents = DateComponents()
//              maxdateComponents.year = 50
//              let minDate = Calendar.current.date(byAdding: mindateComponents, to: currentDate)
//              let maxDate = Calendar.current.date(byAdding: maxdateComponents, to: currentDate)
//
//              let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                                buttonColor: DarkBlueColour,
//                                                font: UIFont.init(name: "CenturyGothic", size: 12)!,
//                                                showCancelButton: true)
//
//
//              datePicker.show("Please Select Date",
//                              doneButtonTitle: "Done",
//                              cancelButtonTitle: "Cancel",
//                              minimumDate: minDate,
//                              maximumDate:maxDate ,
//                              datePickerMode: .dateAndTime) { (date) in
//                                  if let dt = date {
//                                  let formatter2 = DateFormatter()
//                                      formatter2.dateFormat = "yyyy-MM-dd"
//
//                                      let str =   formatter2.string(from: dt)
//                                      self.date_tf.text = str
//                                  }
//              }
       
    }
    
    @IBAction func time_act(_ sender: Any) {
//        let currentDate = Date()
//                   var mindateComponents = DateComponents()
//                   mindateComponents.year = 0
//
//                   var maxdateComponents = DateComponents()
//                   maxdateComponents.year = 50
//                   let minDate = Calendar.current.date(byAdding: mindateComponents, to: currentDate)
//                   let maxDate = Calendar.current.date(byAdding: maxdateComponents, to: currentDate)
//
//                   let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                                     buttonColor: DarkBlueColour,
//                                                     font: UIFont.init(name: "CenturyGothic", size: 12)!,
//                                                     showCancelButton: true)
//
//
//                   datePicker.show("Please Select Time",
//                                   doneButtonTitle: "Done",
//                                   cancelButtonTitle: "Cancel",
//                                   minimumDate: minDate,
//                                   maximumDate:maxDate ,
//                                   datePickerMode: .dateAndTime) { (date) in
//                                       if let dt = date {
//                                       let formatter2 = DateFormatter()
//                                           formatter2.dateFormat = "HH:mm a"
//
//                                           let str =   formatter2.string(from: dt)
//                                           self.time_tf.text = str
//                                       }
//                   }
                
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        date_tf.text = "28/9/2020"
        time_tf.text = "08:30PM - 10PM"
        bookby_tf.text = "Richard Wong Eng Eng"

        SetStsBar()
        
         }
    
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
