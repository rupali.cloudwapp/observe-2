//
//  WebService.swift
//  KCBerry
//
//  Created by Cloudwapp on 08/01/18.
//  Copyright © 2018 Cloudwapp Technologies. All rights reserved.
//

import UIKit
import Alamofire
//import ReachabilitySwift
//import Toaster

protocol WebServiceDelegate: class {
    func serverResponse(data:NSDictionary)
    func failResponse(data:String)
}

class WebService: NSObject {
    
    weak var webDelegate: WebServiceDelegate?
    let reachability = Reachability()
    
    func getRequestMethodName(methodName: String,parameter: [String: Any], completion: @escaping (Any) -> ()) {
        
        if !checkInternet(){
          //  SwiftLoader.hide()
            return
        }
        
        let apiURl = base_url + methodName
        Alamofire.request(apiURl, method: HTTPMethod.get, parameters: parameter, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
        
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value!)
                    completion(response.result.value as! NSDictionary)
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion((response.result.error?.localizedDescription)!)
                break
            }
        }
    }
    func postRequestJsonType(methodName: String,parameter: [String: Any], completion: @escaping (Any) -> ()) {
           
           if !checkInternet(){
               return
           }
           
           let apiURl = base_url + methodName
           
           Alamofire.request(apiURl, method: HTTPMethod.post, parameters: parameter, encoding: JSONEncoding.default)
               .responseJSON { (response:DataResponse<Any>) in
                   
                   switch(response.result) {
                   case .success(_):
                       
                       if response.result.value != nil{
                           print(response.result.value!)
                           completion(response.result.value as! NSDictionary)
                       }
                       
                       break
                       
                   case .failure(_):
                       print(response.result.error!)
                       //Toast(text: (response.result.error?.localizedDescription)!).show()
                       completion((response.result.error?.localizedDescription)!)
                       
                       break
                   }
           }
           
       }
 
    func postRequest(methodName: String,parameter: [String: Any], completion: @escaping (Any) -> ()) {
        
        if !checkInternet(){
            return
        }
        
        let apiURl = base_url + methodName
        
        Alamofire.request(apiURl, method: HTTPMethod.post, parameters: parameter, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value!)
                    completion(response.result.value as! NSDictionary)
                }
                break
                
            case .failure(_):
                print(response.result.error!)
             //   Toast(text: (response.result.error?.localizedDescription)!).show()
                completion((response.result.error?.localizedDescription)!)
                
                break
            }
        }
    }
    
    func uploadImageHandler(methodName: String,parameter: [String: Any],imageData:Data?,imageKey: String, completion: @escaping (Any) -> ()) {
        
        if !checkInternet(){
            return
        }
        
        let url = base_url + methodName
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]

        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                print("insideimage1")

                //                if key == "image"{
                //                     multipartFormData.append(value as!Data, withName: "image", fileName: "image.png", mimeType: "image/png")
                //                }
                //                else{
                //                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                //                }
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "ddMMyyhhmmss"
            let imgPath = "\(dateFormatter.string(from: Date())).jpeg"
            
            if let data = imageData{
              multipartFormData.append(data, withName: imageKey, fileName: imgPath, mimeType: "image/jpeg")
            
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil{
                        print(response.result.value!)
                        completion(response.result.value as! NSDictionary)
                    }
                    print("Succesfully uploaded")
                    if let err = response.error{
                        print(err)
                        completion(err.localizedDescription)
                        //onError?(err)
                        return
                    }
                    //onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completion(error.localizedDescription)
            }
        }
    }
    
    func uploadImagesHandler(methodName: String,parameter: [String: Any], completion: @escaping (Any) -> ()) {
        
        if !checkInternet(){
            return
        }
        
        let url = base_url + methodName
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/mixed"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                                if let newData = value as? Data {
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "ddMMyyyyhhmmss"
                                    let path = dateFormatter.string(from: Date())//"\().jpeg"
                                    
                                    if key == "image" {
                                     //   print("insideimage2")
                                        
                                        
                                        multipartFormData.append(newData, withName: "image", fileName: "\(path).jpeg", mimeType: "image/jpeg")
                                    }
                                    else if key == "profile" {
                                        multipartFormData.append(newData, withName: "profile", fileName: "\(path).jpeg", mimeType: "image/jpeg")
                                    }
                                    else if key == "video" {
                                         multipartFormData.append(newData, withName: "msg", fileName: "\(path).mov", mimeType: "video/mov")
                                    }
                                    else {
                                        multipartFormData.append(newData, withName: "msg", fileName: "\(path).m4a", mimeType: "audio/m4a")
                                    }
                                    
                                }
                                else if let newData = value as? [Data] {

                                    for imageData in newData {
                                        multipartFormData.append(imageData, withName: "\(key)", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                                    }
                                }
                                else if key == "doc"{
                                    if let newData = value as? NSMutableArray {
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "ddMMyyyyhhmmss"
                                        
                                        let imgPath = "\(dateFormatter.string(from: Date()))"
                                        
                                        for imageData in newData {
                                            
                                            multipartFormData.append((imageData as! NSDictionary).value(forKey: "data") as! Data, withName: "document[]", fileName: "\(imgPath)\((imageData as! NSDictionary).value(forKey: "name") as! String)", mimeType: "doc/\((imageData as! NSDictionary).value(forKey: "exten") as! String)")
                                        }
                                    }
                                }
                                else{
                                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil{
                        print(response.result.value!)
                        completion(response.result.value as! NSDictionary)
                    }
                    print("Succesfully uploaded")
                    if let err = response.error{
                        print(err)
                        completion(err.localizedDescription)
                        //onError?(err)
                        return
                    }
                    //onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completion(error.localizedDescription)
            }
        }
    }
    
    func uploadImage(methodName: String,parameter: [String: Any],imageData:Data?) {
        let url = base_url + methodName
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                //                if key == "image"{
                //                     multipartFormData.append(value as!Data, withName: "image", fileName: "image.png", mimeType: "image/png")
                //                }
                //                else{
                //                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                //                }
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "ddMMyyyyhhmmss"
            let imgPath = "\(dateFormatter.string(from: Date())).jpeg"
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: imgPath, mimeType: "image/jpeg")
                //multipartFormData.append(data, withName: "image")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil{
                        print(response.result.value!)
                        self.webDelegate?.serverResponse(data: response.result.value as! NSDictionary)
                    }
                    print("Succesfully uploaded")
                    if let err = response.error{
                        print(err)
                        self.webDelegate?.failResponse(data: err.localizedDescription)
                        
                        //onError?(err)
                        return
                    }
                    //onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                self.webDelegate?.failResponse(data: error.localizedDescription)
                //onError?(error)
            }
        }
    }
    
    func uploadVideoHandler(methodName: String,parameter: [String: Any],videoData:Data?, completion: @escaping (Any) -> ()) {
        
        if !checkInternet(){
            return
        }
        
        let url = base_url + methodName
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

            if let data = videoData {
                multipartFormData.append(data, withName: "video", fileName: "video.mov", mimeType: "video/mov")

            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil{
                        print(response.result.value!)
                        completion(response.result.value as! NSDictionary)
                    }
                    print("Succesfully uploaded")
                    if let err = response.error{
                        print(err)
                        completion(err.localizedDescription)
                        //onError?(err)
                        return
                    }
                    //onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completion(error.localizedDescription)
            }
        }
    }
    
    func uploadDocumentHandler(methodName: String,parameter: [String: Any],docData:Data?,docType:String, completion: @escaping (Any) -> ()) {
        
        if !checkInternet(){
            return
        }
        
        let url = base_url + methodName
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = docData {
                multipartFormData.append(data, withName: "doc", fileName: "doc.\(docType)", mimeType: "video/mov")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.value != nil{
                        print(response.result.value!)
                        completion(response.result.value as! NSDictionary)
                    }
                    print("Succesfully uploaded")
                    if let err = response.error{
                        print(err)
                        completion(err.localizedDescription)
                        //onError?(err)
                        return
                    }
                    //onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completion(error.localizedDescription)
            }
        }
    }
    
    func serverResponse(data: NSDictionary) {
        print(data)
    }
    
    func failResponse(data: String) {
        print(data)
    }
    
    func checkInternet() -> Bool {
        if reachability?.currentReachabilityStatus.description == "No Connection" {
         //   Toast(text: "Internet Connection not Available!").show()
            
       //  let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            
          //  alert.show()
            
        // SwiftLoader.hide()
            
            
        }else{
            //Toast(text: "Internet Connection Available!").show()          
            return true
        }
        return false
    }
    
    func urlRequestWithComponentsForUploadMultipleImages(urlString:String, parameters:[String: Any], imagesData:[Data], imageName: String,completion: @escaping (Any) -> ()) {
        
        // create url request to send
        var mutableURLRequest = URLRequest(url: NSURL(string: urlString)! as URL)
        
        mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        var uploadData = Data()
        // add image
        for data in imagesData {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(imageName)\"; filename=\"\(Date().timeIntervalSince1970).jpeg\"\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append(data)
        }
        // add parameters
        for (key, value) in parameters {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        print("upload",parameters)
        mutableURLRequest.httpBody = uploadData
        
        URLSession.shared.dataTask(with:mutableURLRequest) { (data, response, error) in
            if error != nil {
                
                print(error!)
                completion("")
            } else {
                var dictonary:NSDictionary?
                do {
                    dictonary = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    if let myDictionary = dictonary
                    {
                        completion(myDictionary)
                    }
                } catch let error as NSError {
                    completion(error)
                }
            }
            
            }.resume()

    }
}
