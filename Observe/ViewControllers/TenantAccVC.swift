//
//  TenantAccVC.swift
//  Observe
//
//  Created by USER on 20/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
class TenantAccVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var tenantData = NSArray()
    
    // MARK: - IBOoutlets :-

    
    @IBOutlet weak var list_table: UITableView!
    
    @IBOutlet weak var list_height: NSLayoutConstraint!
    // MARK: - IBActions :-
          
          
    @IBAction func back(_ sender: Any) {
        
        print("back")
             self.navigationController?.popViewController(animated: true)
        
        
//        if #available(iOS 13.0, *) {
//
//                                                                       let scene = UIApplication.shared.connectedScenes.first
//                                                                  if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
//
//
//                                                         (((sd.window?.rootViewController as! UINavigationController).viewControllers[0]) as! TabbarVC).selectedIndex = 3
//                                                                  }
//                                                             }
//                                                             else  {
//                                                            (((appDele.window?.rootViewController as! UINavigationController).viewControllers[0]) as! TabbarVC).selectedIndex = 3
//                                                                 }
//
        
          }
    
    
    @IBAction func req_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReqTenAccVC") as! ReqTenAccVC
                                   
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        SetStsBar()
        self.list_table.delegate = self
                                              self.list_table.dataSource = self
      
                    list_table.separatorStyle = .none
        
        
        
        TeanantList()
        
         }
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            }
    
    
    
    // MARK: - Table view methods:-
       
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row <= self.tenantData.count - 1
        {
            return 120

        }
        else
        {
            return 70


        }
        return 120

       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenantData.count + 1
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = list_table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
       print("self.tenantData.count",self.tenantData.count)
        
        cell.book_btn.isUserInteractionEnabled = false
        
       if self.tenantData.count != 0
       {
        
        if indexPath.row <= self.tenantData.count - 1
        {
            let dict = self.tenantData[indexPath.row] as! NSDictionary
                   
                   cell.heading_lbl.text = (dict.value(forKey: "name") as! String)
                   
                   let tenfrom = dict.value(forKey: "tenant_from") as! String
                      let tento = dict.value(forKey: "tenant_to") as! String
                   
                   cell.desc_lbl.text = tenfrom + " - " + tento
            cell.designable_vw.isHidden = false
            cell.book_height.constant = 0
            cell.book_btn.isHidden = true
        }
        else
        {
            cell.designable_vw.isHidden = true
                    cell.book_height.constant = 45
            cell.book_btn.isHidden = false

        }
        
        }
        else
       {
        cell.designable_vw.isHidden = true
                         cell.book_height.constant = 45
                 cell.book_btn.isHidden = false
        
        }
        
        
        

           cell.selectionStyle = .none
           return cell
           
           
       }
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              if indexPath.row <= self.tenantData.count - 1
              {
               
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "TenanatAccDetailVC") as! TenanatAccDetailVC
                                 
      self.navigationController?.pushViewController(vc, animated: true)
        }
        else
              {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReqTenAccVC") as! ReqTenAccVC
                                              
                   self.navigationController?.pushViewController(vc, animated: true)
                   
        }
                      
                     
                    
       }
    
    // MARK: - RequestAcc LISTING :

          func  TeanantList()
                {
                     let type = NVActivityIndicatorType.ballRotate
                                            let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                            let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                   self.view.addSubview(activityIndicatorView)
                                  self.view.isUserInteractionEnabled = false
                     
                    
                         activityIndicatorView.startAnimating()
                         var param = [String:Any]()
               
                 
                  param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                 


                         WebService().postRequest(methodName: tenant_list , parameter: param) { (response) in
                             
                             activityIndicatorView.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                             
                             if let newResponse = response as? NSDictionary {
                                 if newResponse.value(forKey: "status") as! Bool  == true {
                                     if let userData = newResponse.value(forKey: "data") as? NSArray {
                                          
                                        
                                        self.tenantData = userData
                                        
                                     //   self.list_height.constant = CGFloat(self.tenantData.count * 120)

                                      
                                    self.list_table.reloadData()
                                    }
                                  
                                 }
                                 else {
                                     self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                 }
                             }
                             else if let newMsg = response as? String{
                                self.view.showToast(toastMessage: newMsg, duration: 1)
                             }
                             else {
                                self.view.showToast(toastMessage: "No Data Found", duration: 1)

                             }
                         }
                     
                }

    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
