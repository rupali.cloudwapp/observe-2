//
//  AppDelegate.swift
//  Observe
//
//  Created by USER on 18/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
        let decviceId =  UIDevice.current.identifierForVendor!.uuidString
                           print("decviceId",decviceId)
                           UserDefaults.standard.set(decviceId, forKey: "decviceId")
        
        return true
    }
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
            
         
            
            if #available(iOS 13.0, *) {
              
              
                  } else {
                if UserDefaults.standard.value(forKey: "userIdLogin") != nil
                           {
                               setHomeRootController()

                           }
                           else
                           {

                                setLoginRootController()
                           }
                       
            
             
              
              
            }
            
      
          
        return true
            
            
        }
    
// MARK: For setting roots :-

    func setLoginRootController()  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var rootController = UIViewController()
        
        rootController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        
        let navigationView = UINavigationController(rootViewController: rootController)
        navigationView.navigationBar.isHidden = true
        
        if let window = self.window {
            window.rootViewController = navigationView
        }
    }
    func setHomeRootController()  {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           var rootController = UIViewController()

           rootController = storyboard.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC

           let navigationView = UINavigationController(rootViewController: rootController)
           navigationView.navigationBar.isHidden = true

           if let window = self.window {
               window.rootViewController = navigationView
           }
       }
    
    
    // MARK: UISceneSession Lifecycle
    
 

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

