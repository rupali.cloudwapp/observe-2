//
//  PayNowVC.swift
//  Observe
//
//  Created by USER on 21/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit

class PayNowVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    
    
    // MARK: - IBOoutlets :-

    
    @IBOutlet weak var list_table: UITableView!
    
    // MARK: - IBActions :-
          
          
    @IBAction func back(_ sender: Any) {
              self.navigationController?.popViewController(animated: true)
          }
    
   
    
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        SetStsBar()
        
        list_table.delegate = self
                    list_table.dataSource = self
                    list_table.separatorStyle = .none
        
     
        
         }
         func SetStsBar()
         {
            if #available(iOS 13.0, *) {
                                            let app = UIApplication.shared
                                            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                            
                                            let statusbarView = UIView()
                                statusbarView.backgroundColor = .white
                                            view.addSubview(statusbarView)
                                          
                                            statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                            statusbarView.heightAnchor
                                                .constraint(equalToConstant: statusBarHeight).isActive = true
                                            statusbarView.widthAnchor
                                                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                            statusbarView.topAnchor
                                                .constraint(equalTo: view.topAnchor).isActive = true
                                            statusbarView.centerXAnchor
                                                .constraint(equalTo: view.centerXAnchor).isActive = true
                                          
                                        } else {
                                            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                            statusBar?.backgroundColor =  .white
                                        }
            }
    
    
    
    // MARK: - Table view methods:-
       
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 120
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 2
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = list_table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        if indexPath.row == 0
        {
            
            cell.img_view.image = UIImage.init(named: "online")
            cell.heading_lbl.text = "Online Booking"
            
        }
        else
        {
            cell.img_view.image = UIImage.init(named: "credit")
            cell.heading_lbl.text = "Credit/Debit Card"

        }
        
        
        
           
           cell.selectionStyle = .none
           return cell
           
           
       }
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              
             
//          let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillDetailsVC") as! BillDetailsVC
//                                 
//      self.navigationController?.pushViewController(vc, animated: true)
                      
                     
                    
       }
    

    
    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
