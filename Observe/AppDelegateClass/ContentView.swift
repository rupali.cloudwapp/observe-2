//
//  ContentView.swift
//  Observe
//
//  Created by USER on 18/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

@available(iOS 13.0, *)
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
