//
//  FacilitiesVC.swift
//  Observe
//
//  Created by USER on 19/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class FacilitiesVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
      // MARK: - IBOutlets :
    @IBOutlet weak var noAccess_View: UIView!

    
    @IBOutlet weak var list_table: UITableView!
    @IBOutlet weak var facilitylist_btn: UIButton!
    
    @IBOutlet weak var facility_lbl: UILabel!
    
    @IBOutlet weak var booking_lbl: UILabel!
    
    @IBOutlet weak var booking_btn: UIButton!
    // MARK: - IBActions :

    @IBAction func booking_act(_ sender: Any) {
        
        self.booking_btn.isSelected = true
        self.facilitylist_btn.isSelected = false
        self.facility_lbl.backgroundColor = .gray
        self.booking_lbl.backgroundColor = DarkBlueColour

        self.list_table.reloadData()
    }
    
    @IBAction func facility_act(_ sender: Any) {
        
        self.booking_btn.isSelected = false
             self.facilitylist_btn.isSelected = true
        
        self.facility_lbl.backgroundColor = DarkBlueColour
               self.booking_lbl.backgroundColor = .gray
        self.list_table.reloadData()

    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.value(forKey: "userIdLogin") != nil
                                       {
                                          self.noAccess_View.isHidden = true

                                       }
                                       else
                                       {
                                          
                                          self.noAccess_View.isHidden = false
                                                                                                                

                                       }

     
        list_table.separatorStyle = .none
        self.booking_btn.isSelected = false
                  self.facilitylist_btn.isSelected = true
             
             self.facility_lbl.backgroundColor = DarkBlueColour
                    self.booking_lbl.backgroundColor = .gray
        list_table.delegate = self
             list_table.dataSource = self
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
               swipeLeft.direction = .left
               self.list_table.addGestureRecognizer(swipeLeft)
               
               let swiperight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
               swiperight.direction = .right
               self.list_table.addGestureRecognizer(swiperight)
        
        SetStsBar()
        
    
        
        // Do any additional setup after loading the view.
    }
    
 func SetStsBar()
 {
    if #available(iOS 13.0, *) {
                                    let app = UIApplication.shared
                                    let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                    
                                    let statusbarView = UIView()
                        statusbarView.backgroundColor = .white
                                    view.addSubview(statusbarView)
                                  
                                    statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                    statusbarView.heightAnchor
                                        .constraint(equalToConstant: statusBarHeight).isActive = true
                                    statusbarView.widthAnchor
                                        .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                    statusbarView.topAnchor
                                        .constraint(equalTo: view.topAnchor).isActive = true
                                    statusbarView.centerXAnchor
                                        .constraint(equalTo: view.centerXAnchor).isActive = true
                                  
                                } else {
                                    let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                    statusBar?.backgroundColor =  .white
                                }
    }
    
    
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
          
           if gesture.direction == UISwipeGestureRecognizer.Direction.left
           {
           self.booking_btn.isSelected = true
                            self.facilitylist_btn.isSelected = false
                            self.facility_lbl.backgroundColor = .gray
                            self.booking_lbl.backgroundColor = DarkBlueColour

                            self.list_table.reloadData()
            
           }
           else if gesture.direction == UISwipeGestureRecognizer.Direction.right {
          self.booking_btn.isSelected = false
                                  self.facilitylist_btn.isSelected = true
                             
                             self.facility_lbl.backgroundColor = DarkBlueColour
                                    self.booking_lbl.backgroundColor = .gray
                             self.list_table.reloadData()
                 
         }
           
           
           
       }
    // MARK: - Table view methods:-
       
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if facility_lbl.backgroundColor == DarkBlueColour
        {
            return 200

        }
        else
        {
            return 150

        }
        
           return 150
       }
       
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 2
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = list_table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        
        if facility_lbl.backgroundColor == DarkBlueColour
               {
                
                cell.forward_img.isHidden = true
                cell.book_height.constant = 40
                
                cell.heading_lbl.text = "Recreational Room"
                             cell.desc_lbl.text = "Total area of 1,000 square feet for your use purpose"

               }
               else
               {
                cell.forward_img.isHidden = false
                cell.book_height.constant = 0
                
                cell.heading_lbl.text = "Recreational Room"
                cell.desc_lbl.text = "288/09/20 | 8:30pm - 10:30pm"

               }
        
       
           
           cell.selectionStyle = .none
           return cell
           
           
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if facility_lbl.backgroundColor == DarkBlueColour
               {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
                           
                           self.navigationController?.pushViewController(vc, animated: true)
                
               
                }
        else
        {
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailsVC") as! BookingDetailsVC
                       
                       self.navigationController?.pushViewController(vc, animated: true)
            
           
        }
    
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
