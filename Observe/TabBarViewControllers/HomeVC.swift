//
//  HomeVC.swift
//  Observe
//
//  Created by USER on 19/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import ImageSlideshow
import NVActivityIndicatorView

@available(iOS 13.0, *)
class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,ImageSlideshowDelegate {
    
  var highlightData = NSArray()
    
   // MARK: - IBOutlets :-
    
    @IBOutlet weak var loc_img: UIImageView!
    @IBOutlet weak var home_table: UITableView!
    
    @IBOutlet weak var loc_lbl: UILabel!
    
    @IBOutlet weak var slideShow_view: ImageSlideshow!
    
  // MARK: - IBActions :-
    
    @IBOutlet weak var scroll_view: UIScrollView!
    
    @IBAction func loc_act(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectHomeVC") as! SelectHomeVC
                   
                   self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func call_act(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactSVC") as! ContactSVC
                   
                   self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bills_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillsVC") as! BillsVC
                         
                         self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func visitors_act(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VisitorsVC") as! VisitorsVC
                         
                         self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func report_act(_ sender: Any) {
    }
    
    var slidingArray = NSArray()
       var Topsd_images: [SDWebImageSource] = []
    
    var propertyidstr = String()
      var blockUnitid = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        home_table.separatorStyle = .none

          self.slideShow_view.slideshowInterval = 5.0
                                self.slideShow_view.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
                                self.slideShow_view.contentScaleMode = UIView.ContentMode.scaleToFill
                                
                                let pageControl = UIPageControl()
                                pageControl.currentPageIndicatorTintColor = DarkBlueColour
                                pageControl.pageIndicatorTintColor = .gray
                                self.slideShow_view.pageIndicator = pageControl
                           
                                self.slideShow_view.activityIndicator = DefaultActivityIndicator()
                                self.slideShow_view.currentPageChanged = { page in
                                   // print("current page:", page)
                                }
        
        slideShow_view.setImageInputs([
            ImageSource(image: UIImage(named: "5")!),ImageSource(image: UIImage(named: "14")!),ImageSource(image: UIImage(named: "3")!),ImageSource(image: UIImage(named: "4")!)
     
        ])
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeVC.didTap))
         slideShow_view.addGestureRecognizer(gestureRecognizer)
        
        
        
                                
//                                for image in self.slidingArray {
//                                    let slot = img_Url + ((image as Any) as! String)
//                                    print("slot", slot)
//
//        //                            if canOpenURL(string: slot)
//        //                            {
//                                      self.Topsd_images.append(SDWebImageSource(url: NSURL(string: slot )! as URL))
//                                  //  }
//        //                            else
//        //                            {
//        //                                Toast.init(text: "Image is not in proper format")
//        //                            }
//                                }
//
//                                self.imageSlideShowVw.setImageInputs(self.Topsd_images)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.HighlightList()

         if UserDefaults.standard.value(forKey: "SelectedProperty") != nil
               {
                
                   
                let selectedproperty = UserDefaults.standard.value(forKey: "SelectedProperty") as! NSDictionary
                print("selectedproperty",selectedproperty)
                loc_lbl.textAlignment = .left
                
                let property_name = selectedproperty.value(forKey: "property_name") as! String
                let block = selectedproperty.value(forKey: "block") as! String
                let units = selectedproperty.value(forKey: "units") as! String

                loc_lbl.text = property_name + " , Block " + block + " , Unit " + units
                
                propertyidstr = selectedproperty.value(forKey: "property_id") as! String
                blockUnitid = selectedproperty.value(forKey: "block_unit_id") as! String
                
                self.loc_img.isHidden = false
               }
               else
               {
                
                
                   self.loc_img.isHidden = false

               }
        self.scroll_view.contentSize.height = CGFloat(350 + 150*self.highlightData.count)

    }
    
    override func viewDidLayoutSubviews() {
        
        self.scroll_view.contentSize.height = CGFloat(350 + 150*self.highlightData.count)

    }
    @objc func didTap() {
     slideShow_view.presentFullScreenController(from: self)
    }
    
    // MARK: - Table view methods:-
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.highlightData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = home_table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        let dict = self.highlightData[indexPath.row] as! NSDictionary
         cell.heading_lbl.text = dict.value(forKey: "title") as! String
                cell.desc_lbl.text = dict.value(forKey: "description") as! String
                
       let image = dict.value(forKey: "image") as! String
       
       cell.img_view.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
        
        cell.selectionStyle = .none
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.highlightData[indexPath.row] as! NSDictionary

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HightlioghtsDetailVC") as! HightlioghtsDetailVC
        vc.dict = dict
              
              self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
     // MARK: - RequestAcc LISTING :

                 func HighlightList()
                       {
                            let type = NVActivityIndicatorType.ballRotate
                                                   let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                   let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                          self.view.addSubview(activityIndicatorView)
                                         self.view.isUserInteractionEnabled = false
                            
                           
                                activityIndicatorView.startAnimating()
                                var param = [String:Any]()
                      

//                         param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
//
//                        param["property_id"] = self.propertyidstr
//                        param["block_unit_id"] = self.blockUnitid

                  

                                WebService().postRequest(methodName: highlight , parameter: param) { (response) in
                                    
                                    activityIndicatorView.stopAnimating()
                                   self.view.isUserInteractionEnabled = true
                                    
                                    if let newResponse = response as? NSDictionary {
                                        if newResponse.value(forKey: "status") as! Bool  == true {
                                            if let userData = newResponse.value(forKey: "data") as? NSArray {
                                                 
                                                self.highlightData = userData
                                             self.home_table.delegate = self
                                              self.home_table.dataSource = self
                                                
                                           self.home_table.reloadData()
                                           }
                                         
                                        }
                                        else {
                                            self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                        }
                                    }
                                    else if let newMsg = response as? String{
                                       self.view.showToast(toastMessage: newMsg, duration: 1)
                                    }
                                    else {
                                       self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                    }
                                }
                            
                       }
        


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
