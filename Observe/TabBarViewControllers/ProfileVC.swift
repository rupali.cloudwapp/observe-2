//
//  ProfileVC.swift
//  Observe
//
//  Created by USER on 19/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
class ProfileVC: UIViewController {
    // MARK: - IBOutlets :-
       
    @IBOutlet weak var name_lbl: UILabel!
    
    @IBOutlet weak var noAccess_View: UIView!
    
     // MARK: - IBActions :-
    
    
    @IBAction func payment_act(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentlistVC") as! PaymentlistVC
                                    
                                    self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tenant_act(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TenantAccVC") as! TenantAccVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func helpCentre_act(_ sender: Any) {
    }
    
    @IBAction func terms_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
               
               self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func privacy_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
                   
                   self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logout_act(_ sender: Any) {
        let alertController = UIAlertController(title: "Logout Alert", message: "Are you sure, you want to logout", preferredStyle: .alert)
          let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                  UIAlertAction in
                  NSLog("OK Pressed")
            
            UserDefaults.standard.removeObject(forKey: "userIdLogin")
            UserDefaults.standard.removeObject(forKey: "userData")
            UserDefaults.standard.removeObject(forKey: "PropertyIdArr")
            UserDefaults.standard.removeObject(forKey: "SelectedProperty")

            
            

       if #available(iOS 13.0, *) {
                let scene = UIApplication.shared.connectedScenes.first
            
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                 
                    sd.setLoginRootController()
                }
            }

            else
            {
              appDele.setLoginRootController()
                                                                                                  
                 }

          
          }
          let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.destructive) {
              UIAlertAction in
              NSLog("Cancel Pressed")
              alertController.dismiss(animated: true, completion: nil)
          }
          alertController.addAction(okAction)
          alertController.addAction(cancelAction)
          self.present(alertController, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UserDefaults.standard.value(forKey: "userIdLogin") != nil
                                 {
                                    self.noAccess_View.isHidden = true
                                    
                                   self.getDetails()

                                 }
                                 else
                                 {
                                    
                                    self.noAccess_View.isHidden = false
                                    

                                 }

   SetStsBar()
    }
    
    func SetStsBar()
    {
       if #available(iOS 13.0, *) {
                                       let app = UIApplication.shared
                                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                       
                                       let statusbarView = UIView()
                           statusbarView.backgroundColor = .white
                                       view.addSubview(statusbarView)
                                     
                                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                       statusbarView.heightAnchor
                                           .constraint(equalToConstant: statusBarHeight).isActive = true
                                       statusbarView.widthAnchor
                                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                       statusbarView.topAnchor
                                           .constraint(equalTo: view.topAnchor).isActive = true
                                       statusbarView.centerXAnchor
                                           .constraint(equalTo: view.centerXAnchor).isActive = true
                                     
                                   } else {
                                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                       statusBar?.backgroundColor =  .white
                                   }
       }
    func  getDetails()
     {
          let type = NVActivityIndicatorType.ballRotate
                                 let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                 let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                        self.view.addSubview(activityIndicatorView)
                       self.view.isUserInteractionEnabled = false
          
         
              activityIndicatorView.startAnimating()
              var param = [String:Any]()
    
      
       param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
      


              WebService().postRequest(methodName: get_detail , parameter: param) { (response) in
                  
                  activityIndicatorView.stopAnimating()
                 self.view.isUserInteractionEnabled = true
                  
                  if let newResponse = response as? NSDictionary {
                      if newResponse.value(forKey: "status") as! Bool  == true {
                          if let userData = newResponse.value(forKey: "data") as? NSDictionary {
                               
                             
                            self.name_lbl.text = userData.value(forKey: "name") as! String
                              
                         }
                       
                      }
                      else {
                          self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                      }
                  }
                  else if let newMsg = response as? String{
                     self.view.showToast(toastMessage: newMsg, duration: 1)
                  }
                  else {
                     self.view.showToast(toastMessage: "No Data Found", duration: 1)

                  }
              }
          
     }

       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
