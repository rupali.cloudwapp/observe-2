//
//  NoticeVC.swift
//  Observe
//
//  Created by USER on 19/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import AlamofireImage

class NoticeVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var propertyidstr = String()
    var blockUnitid = String()

    var notiData = NSArray()
    
    
    // MARK: - Iboutlets
    @IBOutlet weak var noAccess_View: UIView!

    
    @IBOutlet weak var list_table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         
             list_table.separatorStyle = .none
        
        if UserDefaults.standard.value(forKey: "userIdLogin") != nil
                                       {
                                          self.noAccess_View.isHidden = true

                                       }
                                       else
                                       {
                                          
                                          self.noAccess_View.isHidden = false
                                                                                                                

                                       }
        
    //    noticeList()
        
       //  getDetails()

        
        
       SetStsBar()
    }
    override func viewWillAppear(_ animated: Bool) {
          if UserDefaults.standard.value(forKey: "SelectedProperty") != nil
                       {
     
                           
                        let selectedproperty = UserDefaults.standard.value(forKey: "SelectedProperty") as! NSDictionary
                        print("selectedproperty",selectedproperty)
                        
                        propertyidstr = selectedproperty.value(forKey: "property_id") as! String
                        blockUnitid = selectedproperty.value(forKey: "block_unit_id") as! String
                        noticeList()
                       }
                       else
                       {
                        

                       }
    }
    func SetStsBar()
    {
       if #available(iOS 13.0, *) {
                                       let app = UIApplication.shared
                                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                       
                                       let statusbarView = UIView()
                           statusbarView.backgroundColor = .white
                                       view.addSubview(statusbarView)
                                     
                                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                       statusbarView.heightAnchor
                                           .constraint(equalToConstant: statusBarHeight).isActive = true
                                       statusbarView.widthAnchor
                                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                       statusbarView.topAnchor
                                           .constraint(equalTo: view.topAnchor).isActive = true
                                       statusbarView.centerXAnchor
                                           .constraint(equalTo: view.centerXAnchor).isActive = true
                                     
                                   } else {
                                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                       statusBar?.backgroundColor =  .white
                                   }
       }
    // MARK: - Table view methods:-
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notiData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = list_table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.selectionStyle = .none
        
        let dict = self.notiData[indexPath.row] as! NSDictionary
        
        cell.heading_lbl.text = dict.value(forKey: "title") as! String
        cell.desc_lbl.text = dict.value(forKey: "description") as! String
        
        
//        let image = dict.value(forKey: "image") as! String
//
//        cell.img_view.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)

        

        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
          let dict = self.notiData[indexPath.row] as! NSDictionary

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NoticeDetailVC") as! NoticeDetailVC
        
        vc.dict = dict
                              
           self.navigationController?.pushViewController(vc, animated: true)
                   
                  
                 
    }
    
    // MARK: - RequestAcc LISTING :

             func  noticeList()
                   {
                        let type = NVActivityIndicatorType.ballRotate
                                               let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                               let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                      self.view.addSubview(activityIndicatorView)
                                     self.view.isUserInteractionEnabled = false
                        
                       
                            activityIndicatorView.startAnimating()
                            var param = [String:Any]()
                  

                     param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")

                    param["property_id"] = self.propertyidstr
                    param["block_unit_id"] = self.blockUnitid

                        
//                                         param["user_id"] = "1"
//
//                                        param["property_id"] = "3"
//                                        param["block_unit_id"] = "3"


                            WebService().postRequest(methodName: notification , parameter: param) { (response) in
                                
                                activityIndicatorView.stopAnimating()
                               self.view.isUserInteractionEnabled = true
                                
                                if let newResponse = response as? NSDictionary {
                                    if newResponse.value(forKey: "status") as! Bool  == true {
                                        if let userData = newResponse.value(forKey: "data") as? NSArray {
                                             
                                            self.notiData = userData
                                         self.list_table.delegate = self
                                          self.list_table.dataSource = self
                                            
                                       self.list_table.reloadData()
                                       }
                                     
                                    }
                                    else {
                                        self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                    }
                                }
                                else if let newMsg = response as? String{
                                   self.view.showToast(toastMessage: newMsg, duration: 1)
                                }
                                else {
                                   self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                }
                            }
                        
                   }
    

       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
