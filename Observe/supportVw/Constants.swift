//
//  Constants.swift
//  Teammates.net
//
//  Created by CP-02 on 05/12/18.
//  Copyright © 2018 CP-02. All rights reserved.
//

import Foundation
import AVKit




let google_APIkey = "AIzaSyDpLGc9pTTxvx4qAraD9bPV7oDgfBIpnj8"

let googleLanguage = "en"

let img_Url = "http://mobidudes.com/vertical/"
let base_url = "http://mobidudes.com/vertical/api/H1/"

let RegistrationService = "signup"
let LoginService = "login"
let logout = "logout"
let get_detail = "get_detail"
let property_list = "property_list"
let user_property_list = "user_property_list"
let visitor_add = "visitor_add"

let report = "report"
let banner = "banner"
let highlight = "highlight"
let tenant_add = "tenant_add"
let tenant_list = "tenant_list"
let visitor_list = "visitor_list"

let terms = "terms"
let privacy = "privacy"
let notification = "notification"
let facility_list = "facility_list"
let facility_booking = "facility_booking"


@available(iOS 13.0, *)
var appDele = UIApplication.shared.delegate as! AppDelegate



//let themeColorFaint = hexStringToUIColor (hex:"8eb1cd")

let themeColorFaint = UIColor.init(red: 113/255.0, green: 145/255.0, blue: 166/255.0, alpha: 1)
let DarkBlueColour = hexStringToUIColor (hex:"1246FF")
let LightBlueColour = hexStringToUIColor (hex:"1EC3E7")


let thgreen = UIColor.init(red: 2/255.0, green: 162/255.0, blue: 95/255.0, alpha: 1)



func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
