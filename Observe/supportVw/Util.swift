//
//  Utility.swift
//  StudentManagement
//
//  Created by CP-02 on 30/10/18.
//  Copyright © 2018 CP-02. All rights reserved.
//

import UIKit
import ImageIO
//public protocol ChangableFont: AnyObject {
//    var text: String? { get set }
//    var attributedText: NSAttributedString? { get set }
//    var rangedAttributes: [RangedAttributes] { get }
//    func getFont() -> UIFont?
//    func changeFont(ofText text: String, with font: UIFont)
//    func changeFont(inRange range: NSRange, with font: UIFont)
//    func changeTextColor(ofText text: String, with color: UIColor)
//    func changeTextColor(inRange range: NSRange, with color: UIColor)
//    func resetFontChanges()
//}
//
//public struct RangedAttributes {
//
//    let attributes: [NSAttributedString.Key: Any]
//    let range: NSRange
//
//    public init(_ attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
//        self.attributes = attributes
//        self.range = range
//    }
//}

class Util: NSObject {
 
    class func invokeAlertMethod(strTitle: NSString, strBody: NSString, delegate: AnyObject?) {
        let alert: UIAlertView = UIAlertView()
        alert.message = strBody as String
        alert.title = strTitle as String
        alert.delegate = delegate
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
}

extension String {
    func strToDate(format:String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)!
    }
    
    func heightForView(font:UIFont, width:CGFloat) -> CGFloat{
        
        let label:UILabel = UILabel(frame:  CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = self
        
        label.sizeToFit()
        return label.frame.height
    }
}

extension Date {
    func dateToStr(format:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension String {
    func checkNull() -> Bool {
        if self.isEmpty{
            return true
        }
        return false
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let passwordRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    func verifyUrl(urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url = URL(string: urlString) {
                return UIApplication.shared.canOpenURL(url)
            }
        }
        return false
    }
    
    func checkValidUrl() -> Bool {
        let urlRegEx: String = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        return urlTest.evaluate(with: self)
    }
    
    func verifyUrl() -> Bool {
        if let url = URL(string: self) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    func isEmptyOrWhitespace() -> Bool {
        if(self.isEmpty) {
            return true
        }
        return (self.trimmingCharacters(in: NSCharacterSet.whitespaces) == "")
    }
    
//    var html2AttributedString: NSAttributedString? {
//        return Data(utf8).html2AttributedString
//    }
//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
    
    func stringToJson() -> Any {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        return (Any).self
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}


extension UIView
{
func showToast(toastMessage:String,duration:CGFloat)
{
//View to blur bg and stopping user interaction
let bgView = UIView(frame: self.frame)
bgView.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(255.0/255.0), blue: CGFloat(255.0/255.0), alpha: CGFloat(0.6))
bgView.tag = 555

//Label For showing toast text
let lblMessage = UILabel()
lblMessage.numberOfLines = 0
lblMessage.lineBreakMode = .byWordWrapping
lblMessage.textColor = .white
lblMessage.backgroundColor = .black
lblMessage.textAlignment = .center
lblMessage.font = UIFont.init(name: "Helvetica Neue", size: 17)
lblMessage.text = toastMessage

//calculating toast label frame as per message content
let maxSizeTitle : CGSize = CGSize(width: self.bounds.size.width-16, height: self.bounds.size.height)
var expectedSizeTitle : CGSize = lblMessage.sizeThatFits(maxSizeTitle)
// UILabel can return a size larger than the max size when the number of lines is 1
expectedSizeTitle = CGSize(width:maxSizeTitle.width.getminimum(value2:expectedSizeTitle.width), height: maxSizeTitle.height.getminimum(value2:expectedSizeTitle.height))
lblMessage.frame = CGRect(x:((self.bounds.size.width)/2) - ((expectedSizeTitle.width+16)/2) , y: (self.bounds.size.height/2) - ((expectedSizeTitle.height+16)/2), width: expectedSizeTitle.width+16, height: expectedSizeTitle.height+16)
lblMessage.layer.cornerRadius = 8
lblMessage.layer.masksToBounds = true
lblMessage.padding = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
bgView.addSubview(lblMessage)
self.addSubview(bgView)
lblMessage.alpha = 0

UIView.animateKeyframes(withDuration:TimeInterval(duration) , delay: 0, options: [] , animations: {
lblMessage.alpha = 1
}, completion: {
sucess in
UIView.animate(withDuration:TimeInterval(duration), delay: 8, options: [] , animations: {
lblMessage.alpha = 0
bgView.alpha = 0
})
bgView.removeFromSuperview()
})
}
}
extension CGFloat
{
func getminimum(value2:CGFloat)->CGFloat
{
if self < value2
{
return self
}
else
{
return value2
}
}
}

//MARK: Extension on UILabel for adding insets - for adding padding in top, bottom, right, left.

extension UILabel
{
private struct AssociatedKeys {
static var padding = UIEdgeInsets()
}

var padding: UIEdgeInsets? {
get {
return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
}
set {
if let newValue = newValue {
    objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
}
}
}

override open func draw(_ rect: CGRect) {
if let insets = padding {
    self.drawText(in: rect.inset(by: insets))
} else {
self.drawText(in: rect)
}
}

override open var intrinsicContentSize: CGSize {
get {
var contentSize = super.intrinsicContentSize
if let insets = padding {
contentSize.height += insets.top + insets.bottom
contentSize.width += insets.left + insets.right
}
return contentSize
}
}
}













//
//extension UILabel: ChangableFont {
//
//    public func getFont() -> UIFont? {
//        return font
//    }
//}
//
//extension UITextField: ChangableFont {
//
//    public func getFont() -> UIFont? {
//        return font
//    }
//}

//public extension ChangableFont {
//
//    var rangedAttributes: [RangedAttributes] {
//        guard let attributedText = attributedText else {
//            return []
//        }
//        var rangedAttributes: [RangedAttributes] = []
//        let fullRange = NSRange(
//            location: 0,
//            length: attributedText.string.count
//        )
//        attributedText.enumerateAttributes(
//            in: fullRange,
//            options: []
//        ) { (attributes, range, stop) in
//            guard range != fullRange, !attributes.isEmpty else { return }
//            rangedAttributes.append(RangedAttributes(attributes, inRange: range))
//        }
//        return rangedAttributes
//    }
//
//    func changeFont(ofText text: String, with font: UIFont) {
//
//
//        guard let range = (self.attributedText?.string ?? self.text)?.range(of: text) else { return }
//
//        changeFont(inRange: range, with: font)
//    }
//
//    func changeFont(inRange range: NSRange, with font: UIFont) {
//        add(attributes: [.font: font], inRange: range)
//    }
//
//    func changeTextColor(ofText text: String, with color: UIColor) {
//        guard let range = (self.attributedText?.string ?? self.text)?.range(of: text) else { return }
//        changeTextColor(inRange: range, with: color)
//    }
//
//    func changeTextColor(inRange range: NSRange, with color: UIColor) {
//        add(attributes: [.foregroundColor: color], inRange: range)
//    }
//
//    private func add(attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
//        guard !attributes.isEmpty else { return }
//
//        var rangedAttributes: [RangedAttributes] = self.rangedAttributes
//
//        var attributedString: NSMutableAttributedString
//
//        if let attributedText = attributedText {
//            attributedString = NSMutableAttributedString(attributedString: attributedText)
//        } else if let text = text {
//            attributedString = NSMutableAttributedString(string: text)
//        } else {
//            return
//        }
//
//        rangedAttributes.append(RangedAttributes(attributes, inRange: range))
//
//        rangedAttributes.forEach { (rangedAttributes) in
//            attributedString.addAttributes(
//                rangedAttributes.attributes,
//                range: rangedAttributes.range
//            )
//        }
//
//        attributedText = attributedString
//    }
//
//    func resetFontChanges() {
//        guard let text = text else { return }
//        attributedText = NSMutableAttributedString(string: text)
//    }
//}
//555-610-6679
/*8extension UIImage {
    func compressTo(_ expectedSizeInMb:Int) -> Data? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
           
            if let data:Data = self.jpegData(compressionQuality: compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                return data
            }
        }
        return nil
    }
}*/

extension UIView {
    func setBorder(corner:CGFloat,color:UIColor)  {
        self.layer.cornerRadius = corner
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
    
//    func dropShadow() {
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOpacity = 0.2
//        layer.shadowOffset = .zero
//        layer.shadowRadius = 1
//        layer.shouldRasterize = true
//    }
}

extension UIButton {
    func centerImageAndButton(_ gap: CGFloat, imageOnTop: Bool) {
        
        guard let imageView = self.imageView,
            let titleLabel = self.titleLabel else { return }
        
        let sign: CGFloat = imageOnTop ? 1 : -1;
        let imageSize = imageView.frame.size;
        self.titleEdgeInsets = UIEdgeInsets(top: (imageSize.height+gap)*sign, left: -imageSize.width, bottom: 0, right: 0);
        
        let titleSize = titleLabel.bounds.size;
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height+gap)*sign, left: 0, bottom: 0, right: -titleSize.width);
        
    }
}

extension UIImage{
    
    func compressTo(_ expectedSizeInMb:Int) -> Data? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
           
            if let data:Data =  self.jpegData(compressionQuality: compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                return data
            }
        }
        return nil
    }
    
    public class func gifImageWithData(data: NSData) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            print("image doesn't exist")
            return nil
        }
        
        return UIImage.animatedImageWithSource(source: source)
    }
    
    public class func gifImageWithURL(gifUrl:String) -> UIImage? {
        guard let bundleURL = NSURL(string: gifUrl)
            else {
                print("image named \"\(gifUrl)\" doesn't exist")
                return nil
        }
        guard let imageData = NSData(contentsOf: bundleURL as URL) else {
            print("image named \"\(gifUrl)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    public class func gifImageWithName(name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else {
                print("SwiftGif: This image named \"\(name)\" does not exist")
                return nil
        }
        
        guard let imageData = NSData(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    class func delayForImageAtIndex(index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1
        
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(CFDictionaryGetValue(cfProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()), to: CFDictionary.self)
        
        var delayObject: AnyObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()), to: AnyObject.self)
        
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1
        }
        
        return delay
    }
    
    class func gcdForPair(a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        if a! < b! {
            let c = a!
            a = b!
            b = c
        }
        
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b!
            } else {
                a = b!
                b = rest
            }
        }
    }
    
    class func gcdForArray(array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(a: val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        for i in 0..<count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }
            
            let delaySeconds = UIImage.delayForImageAtIndex(index: Int(i), source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
        }()
        
        let gcd = gcdForArray(array: delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        let animation = UIImage.animatedImage(with: frames, duration: Double(duration) / 1000.0)
        
        return animation
    }
}
