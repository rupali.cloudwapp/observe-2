//
//  TableViewCell.swift
//  Observe
//
//  Created by USER on 19/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var designable_vw: DesignableView!
    @IBOutlet weak var heading_lbl: UILabel!
    
    @IBOutlet weak var desc_lbl: UILabel!
    
    @IBOutlet weak var img_view: UIImageView!
    
    @IBOutlet weak var forward_img: UIImageView!
    
    @IBOutlet weak var book_btn: UIButton!
    
    @IBOutlet weak var book_height: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
