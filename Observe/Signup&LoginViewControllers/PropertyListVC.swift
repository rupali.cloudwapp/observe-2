//
//  PropertyListVC.swift
//  Observe
//
//  Created by USER on 22/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
class PropertyListVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    
     // MARK: - IBDeclarations :
    
       var PropertyDataArr = NSArray()
       var PropertyIdArr = [String]()
    var PropertyNameArr = [String]()

       var blockunitIdArr = [String]()
        var selectindArr = [Int]()

    // MARK: - IBOoutlets :-

       
       @IBOutlet weak var list_table: UITableView!
       
     
       // MARK: - IBActions :-
             
             
    @IBAction func submit_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        
        vc.PropertyIdArr = PropertyIdArr
        vc.blockunitIdArr = blockunitIdArr
        vc.PropertyNameArr = PropertyNameArr
        
        UserDefaults.standard.set(PropertyIdArr, forKey: "PropertyIdArr")
        UserDefaults.standard.set(blockunitIdArr, forKey: "blockunitIdArr")
        UserDefaults.standard.set(PropertyNameArr, forKey: "PropertyNameArr")

       self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func back(_ sender: Any) {
                 self.navigationController?.popViewController(animated: true)
             }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        list_table.delegate = self
        list_table.dataSource = self
        self.list_table.allowsMultipleSelection = true

                PropertyList()

    }
    // MARK: - WebService Property list :
     func  PropertyList()
             {
                  let type = NVActivityIndicatorType.ballRotate
                                         let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                         let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                self.view.addSubview(activityIndicatorView)
                               self.view.isUserInteractionEnabled = false
                  
                 
                      activityIndicatorView.startAnimating()
                      var param = [String:Any]()
            
                 WebService().postRequest(methodName: property_list , parameter: [:]) { (response) in
                     

                     
                          
                          activityIndicatorView.stopAnimating()
                         self.view.isUserInteractionEnabled = true
                          
                          if let newResponse = response as? NSDictionary {
                              if newResponse.value(forKey: "status") as! Bool  == true {
                                  if let propertyData = newResponse.value(forKey: "data") as? NSArray {
                                     print("propertyData",propertyData)
                                    
                                    self.PropertyDataArr = propertyData
                                    
//                                    self.PropertyNameArr = (propertyData.value(forKey: "property_name") as? [String])!
//                                   
//                                    self.PropertyIdArr = (propertyData.value(forKey: "property_id") as? [String])!
//                                    self.blockunitIdArr = (propertyData.value(forKey: "block_unit_id") as? [String])!
                                    
                                    print("PropertyIdArr",self.PropertyIdArr)
                                    self.list_table.reloadData()

                                    
                                
                              }
                              else {
                                  self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                              }
                          }
                          }
                          else if let newMsg = response as? String{
                             self.view.showToast(toastMessage: newMsg, duration: 1)
                          }
                          else {
                             self.view.showToast(toastMessage: "No Data Found", duration: 1)

                          }
                      }
                  
             }

      // MARK: - TABLE VIEW METHODS :-
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return 80
            
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return PropertyDataArr.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PropertyCell
            

           let dict = PropertyDataArr[indexPath.row] as! NSDictionary
            
            let name = dict.value(forKey: "property_name") as! String
            let block = dict.value(forKey: "block") as! String
            let unit = dict.value(forKey: "unit") as! String

           cell.name_lbl.text = name + " , " + block + " , " + unit
           

           cell.selectionStyle = .none
            
         //  cell.selectJob_btn.isSelected = false
            
            cell.chcek_btn.isUserInteractionEnabled = false
            
            if selectindArr.contains(indexPath.row)
            {
                  
                cell.chcek_btn.isSelected = true
            }
            else
            {
                cell.chcek_btn.isSelected = false

            }
            
             
            return cell
        
        }
       
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

         //   selectedindex = indexPath.row
            let cell = tableView.cellForRow(at: indexPath) as! PropertyCell
           
            print("selectedindex",indexPath.row)
            
           if cell.chcek_btn.isSelected == true
           {
    print("selectindArrwhiledeselct",selectindArr)

            
            let PropertyId = PropertyIdArr[indexPath.row] as! String
            let blockunitId = blockunitIdArr[indexPath.row] as! String
            let PropertyName = PropertyNameArr[indexPath.row] as! String

                   let selectind = indexPath.row as! Int
            
            PropertyIdArr.removeAll { $0 == PropertyId }
            blockunitIdArr.removeAll { $0 == blockunitId }

            selectindArr.removeAll { $0 == selectind }
           PropertyNameArr.removeAll { $0 == PropertyName }
         
            print("PropertyIdArr",PropertyIdArr)
                      print("deselectindArr",selectindArr)
                   list_table.reloadData()
            }
            
            else
           {
            
            print("selectindArr",selectindArr)

            let dict = PropertyDataArr[indexPath.row] as! NSDictionary
            
            let PropertyId = dict.value(forKey: "property_id") as! String
            let blockunitId = dict.value(forKey: "block_unit_id") as! String
            let PropertyName = dict.value(forKey: "property_name") as! String

            PropertyIdArr.append(PropertyId)
            blockunitIdArr.append(blockunitId)
            PropertyNameArr.append(PropertyName)

            selectindArr.append(indexPath.row)
            
            print("PropertyIdArr",PropertyIdArr)
            print("selectindArr",selectindArr)
            list_table.reloadData()

            }

    
            

        }    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
