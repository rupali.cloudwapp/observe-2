//
//  LoginVC.swift
//  Observe
//
//  Created by USER on 18/07/20.
//  Copyright © 2020 USER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView

@available(iOS 13.0, *)
class LoginVC: UIViewController {
    
    // MARK: - IBOutlets :
    
    
    @IBOutlet weak var email_tf: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var pw_tf: SkyFloatingLabelTextField!
    

    // MARK: - IBActions :

    @IBAction func reset_act(_ sender: Any) {
    }
    
    
    @IBAction func request_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func Login_act(_ sender: Any) {
   
           if !(email_tf.text!.isValidEmail()) {
         self.view.showToast(toastMessage:  "Valid Email Required!!", duration: 1)
                                                }
            
             else if pw_tf.text!.isEmpty {
             
             self.view.showToast(toastMessage:  "Password Required!!", duration: 1)
             
             }
                
          else
            {
            loginUser()
             
     }
        
    }
    
     

    override func viewDidLoad() {
        super.viewDidLoad()

        
        print("login screen")
        
//        for name in UIFont.familyNames {
//          print(name)
//          if let nameString = name as? String
//          {
//            print(UIFont.fontNames(forFamilyName: nameString))
//          }
//        }
        
        
        
        
        
    
    }
    // MARK: - WebService login :

        func loginUser()
              {
                   let type = NVActivityIndicatorType.ballRotate
                                          let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                          let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: DarkBlueColour, padding: 20)
                                 self.view.addSubview(activityIndicatorView)
                                self.view.isUserInteractionEnabled = false
                   
                  
                       activityIndicatorView.startAnimating()
                       var param = [String:Any]()


                let decviceId = UserDefaults.standard.value(forKey: "decviceId")
               param["email"] =  self.email_tf.text as! String
                param["password"] = self.pw_tf.text as! String
                param["fcm_token"] = "test"
                param["device_type"] = "IOS"
                param["device_id"] = decviceId

                
                       WebService().postRequest(methodName: LoginService , parameter: param) { (response) in
                           
                           activityIndicatorView.stopAnimating()
                          self.view.isUserInteractionEnabled = true
                           
                           if let newResponse = response as? NSDictionary {
                               if newResponse.value(forKey: "status") as! Bool  == true {
                                   if let userData = newResponse.value(forKey: "data") as? NSDictionary {
                                        
                                  print("userData",userData)
                                       
                                       let userid = userData.value(forKey: "id") as! String
                         
                                       UserDefaults.standard.set(userid, forKey: "userIdLogin")
                                   UserDefaults.standard.set(userData, forKey: "userData")
                                    
                                   if #available(iOS 13.0, *) {
                                            let scene = UIApplication.shared.connectedScenes.first
                                        
                                            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                             
                                                sd.setHomeRootController()
                                            }
                                        }

                                        else
                                        {
                                          appDele.setHomeRootController()
                                                                                                                              
                                             }
                                    
                                        
                                   }
                               }
                               else {
                                   self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                               }
                           }
                           else if let newMsg = response as? String{
                              self.view.showToast(toastMessage: newMsg, duration: 1)
                           }
                           else {
                              self.view.showToast(toastMessage: "No Data Found", duration: 1)

                           }
                       }
                   
              }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
